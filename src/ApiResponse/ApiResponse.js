class ApiResponse {
    constructor(success, message, data) {
      this.success = success;
      this.message = message;
      this.data = data;
    }
  }
  
  function successResponse(message, data) {
    return new ApiResponse(true, message, data);
  }
  
  function errorResponse(message) {
    return new ApiResponse(false, message);
  }
  
  module.exports = { ApiResponse, successResponse, errorResponse };
  