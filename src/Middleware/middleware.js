const jwt = require('jsonwebtoken');
require('dotenv').config();
const { successResponse, errorResponse } = require('../ApiResponse/ApiResponse');

    const JWT_SECRET = process.env.JWT_SECRET;

function verifyToken(req, res, next) {
  const authHeader = req.headers.authorization;
  if (!authHeader) {
    return res.status(401).json({ message: 'Unauthorized' });
  }
  const token = authHeader.slice(7);
  jwt.verify(token, JWT_SECRET, (err, decoded) => {
    if (err) {
      return res.status(403).json({ message: 'Invalid token' });
    }
    req.user = decoded.user;
    next();
  });
}

function requireAdmin(req, res, next) {
      if (req.user.role_id !== 1) {
        return res.status(403).json({ message: 'Unauthorized: Admin access required' });
      }
      next();
    }
 
   
 

module.exports = {
  verifyToken,
  requireAdmin,
};
