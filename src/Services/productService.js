const Product = require('../Models/Product');
const fs = require('fs');
const fsp = require('fs').promises;

async function createProduct(name, price, imageUrl) {
  // Implement logic to create a new product in the database

  const newProduct = new Product(name, price,imageUrl );

  await newProduct.save();

  // Generate JWT token
  return newProduct;
}

async function getAllProducts() {
  try {

    const products = await Product.find();
    return products; // Return the list of products
  } catch (error) {
    throw new Error('Failed to fetch products'); // Handle error if fetching fails
  }

}
async function getProductById(id) {
  try {
    const product = await Product.findById(id);
    if (product !== null) {
      return product; // Return the found product
    } else {
      throw new Error('No product found');
    }
  } catch (error) {
    throw new Error(error.message); // Handle error if fetching fails
  }
}
async function updateProductById(productData) {
  try {
    const product = await Product.updateById(productData);
    if (product> 0)
    {
      return productData
    }
     ; // Return the list of products
  } catch (error) {
    throw new Error('Failed to update product'); // Handle error if fetching fails
  }

}

async function deleteProductById(id) {
  try {
     await Product.deleteById(id);
    return ; // Return the list of products
  } catch (error) {
    throw new Error('Failed to delete product'); // Handle error if fetching fails
  }

}



async function uploadFile(uploadedFile, uploadDir) {
  try {
    if (!fs.existsSync(uploadDir)) {
      fs.mkdirSync(uploadDir, { recursive: true }); // Create upload directory if it doesn't exist
    }

    const fileUrl = uploadDir + Date.now() + uploadedFile.name; // Generate unique file name

    await uploadedFile.mv('./public' + fileUrl); // Move the uploaded file to the destination directory

    return fileUrl; // Return the URL of the uploaded file
  } catch (error) {
    console.error("Error uploading file:", error);
    throw new Error('Failed to upload file');
  }
}


async function deleteFile(filePath) {
  try {
    console.log("Attempting to delete file:", filePath);

    // Check if the file exists
    if (fs.existsSync(filePath)) {
      // If the file exists, attempt to delete it
      await fsp.unlink(filePath);
      console.log(`File ${filePath} deleted successfully`);
    } else {
      console.log(`File ${filePath} does not exist`);
    }
  } catch (error) {
    console.error("Error deleting file:", error);
    throw new Error(`Failed to delete file: ${error.message}`);
  }
}

// Implement getProductById, updateProductById, and deleteProductById similarly

module.exports = {
  createProduct,
  getAllProducts,
  getProductById,
  updateProductById,
  deleteProductById,
  uploadFile,
  deleteFile,
  // Other service functions
};