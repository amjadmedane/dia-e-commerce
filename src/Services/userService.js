const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../Models/user');
require('dotenv').config();

const JWT_SECRET = process.env.JWT_SECRET;

async function signup(username, password) {
  // Check if the user already exists
  const existingUser = await User.findByUsername(username);
  if (existingUser) {
    throw new Error('User already exists');
  }

  // Create a new user

  const hashedPassword = await bcrypt.hash(password, 10);
  const newUser = new User(username, hashedPassword);

  await newUser.save();


  // Generate JWT token
  return jwt.sign({ username }, JWT_SECRET);
}

async function login(username, password) {
  // Find the user
  const user = await User.findByUsername(username);
  if (!user) {
    throw new Error('Invalid username or password');
  }

// Compare passwords
  const passwordMatch = await bcrypt.compare(password, user.password);
  if (!passwordMatch) {
    throw new Error('Invalid username or password');
  }
  // Generate JWT token
  return {
    token: jwt.sign({ user }, JWT_SECRET),
    user: user
  };
}

module.exports = { signup, login };
