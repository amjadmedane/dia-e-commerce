const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const productController = require('../Controllers/productController');
const { requireAdmin  } = require('../Middleware/middleware'); // Import middleware functions

// Create a new product
router.post('/create',requireAdmin, productController.createProduct);

// Get all products
router.get('/getAll', productController.getAllProducts);

// Get a single product by ID
router.get('/getById/:id', productController.getProductById);

// // Update a product by ID
router.post('/updateById/:id',requireAdmin, productController.updateProductById);

// // Delete a product by ID
router.delete('/deleteById/:id',requireAdmin, productController.deleteProductById);

module.exports = router;