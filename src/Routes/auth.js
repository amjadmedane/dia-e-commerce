const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const UserController = require('../Controllers/userController');
const loginValidateInputs = require('../validationMiddleware/authValidation');


router.post('/signup',loginValidateInputs, UserController.signup);

router.post('/login', loginValidateInputs, UserController.login);


module.exports = router;