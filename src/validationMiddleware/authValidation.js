function loginValidateInputs(req, res, next) {
    const { username, password, email } = req.body;
  
    // Check if username is provided and is at least 3 characters long
    if (!username || username.length < 5) {
      return res.status(400).json({ message: 'Username must be at least 5 characters long' });
    }
  
    // Check if password is provided and is at least 6 characters long
    if (!password || password.length < 6) {
      return res.status(400).json({ message: 'Password must be at least 6 characters long' });
    }
  
    // Check if email is provided and has a valid format
    // if (!email || !isValidEmail(email)) {
    //   return res.status(400).json({ message: 'Please provide a valid email address' });
    // }
  
    // If all validations pass, proceed to the next middleware
    next();
  }
  
  function isValidEmail(email) {
    // Basic email format validation using a regular expression
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }
  
  module.exports = loginValidateInputs;