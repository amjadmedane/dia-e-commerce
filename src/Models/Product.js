const pool = require('../../db');

class Product {
  constructor(name, price,image_url) {
    this.name = name;
    this.price = price;
    this.image_url = image_url;
  }

  async save() {
    try {

      const [rows, fields] = await pool.execute(
        'INSERT INTO products (name, price, image_url) VALUES (?, ?, ?)',
        [this.name, this.price, this.image_url]
      );

      return rows.insertId;
    } catch (error) {
      throw error;
    }
  }
  static async find() {
    try {
      const [rows, fields] = await pool.execute('SELECT * FROM products');
      return rows; // Return the fetched rows directly
    } catch (error) {
      throw error;
    }
  }
  static async findById(id) {
    try {
      const [rows, fields] = await pool.execute('SELECT * FROM products WHERE id = ?', [id]);
      return rows.length > 0 ? rows[0] : null; // Return the first row (object) or null if no rows found
    } catch (error) {
      throw error;
    }
  }
  static async deleteById(id) {
    try {
      await pool.execute('DELETE FROM products WHERE id = ?', [id]);
      return 'Product deleted successfully'; // Return a success message
    } catch (error) {
      throw error;
    }
  }

static async updateById(product) {
    try {
      // Execute an SQL query to update the product details in the database
      const [rows, fields] = await pool.execute(
        'UPDATE products SET name = ?, price = ?, image_url = ? WHERE id = ?',
        [product.name, product.price, product.imageUrl, product.id]
      );
      
      // Return the number of affected rows
      return rows.affectedRows;
    } catch (error) {
      throw error;
    }
  }

  
  
  
}

module.exports = Product;
