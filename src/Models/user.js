const pool = require('../../db');
const bcrypt = require('bcrypt');

class User {
  constructor(username, password) {
    this.username = username;
    this.password = password;
    // this.role_id = "2";

  }

  async save() {
    try {

      const [rows, fields] = await pool.execute(
        'INSERT INTO users (username, password) VALUES (?, ?)',
        [this.username, this.password]
      );

      return rows.insertId;
    } catch (error) {
      throw error;
    }
  }

  static async findByUsername(username) {
    try {
      const [rows, fields] = await pool.execute(
        'SELECT * FROM users WHERE username = ?',
        [username]
      );
      if (rows.length > 0) {
        const user = rows[0];

        return user;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  async comparePassword(password) {
    try {
      return await bcrypt.compare(password, this.password);
    } catch (error) {
      throw error;
    }
  }
}

module.exports = User;
