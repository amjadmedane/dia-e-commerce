const jwt = require('jsonwebtoken');
const UserService = require('../Services/userService');
const { successResponse, errorResponse } = require('../ApiResponse/ApiResponse');

const JWT_SECRET = process.env.JWT_SECRET;

async function signup(req, res) {
  const { username, password } = req.body;
  if (!username || !password) {
    return res.status(400).json({ message: 'Please provide both username and password' });
  }
  try {
    const token = await UserService.signup(username, password);

    res.json(successResponse('Signup successful',  {user: {username} ,token  }));
    
    // res.json({ message: 'Signup successful', token });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}
async function login(req, res) {
  const { username, password } = req.body;
  if (!username || !password) {
    return res.status(400).json({ message: 'Please provide both username and password' });
  }

  try {
    const data = await UserService.login(username, password);
    res.json(successResponse('Signup successful',  {user:{username: data.user.username, role: data.user.role_id}  , token :data.token  }));

    // res.json({ message: 'Login successful', token });
  } catch (error) {
    res.status(401).json(errorResponse(error.message ));

    // res.status(500).json({ message: error.message });
  }
  
}

module.exports = { signup, login };
