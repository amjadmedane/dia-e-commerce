const productService = require('../Services/productService');
const bodyParser = require('body-parser');
const { successResponse, errorResponse } = require('../ApiResponse/ApiResponse');
// const multer = require('multer');
// const upload = multer({ dest: 'public/uploads/productImages' }); // Specify the directory where uploaded files will be stored
const fs = require('fs');
const uploadDir = '/uploads/productImages/';



async function createProduct(req, res) {
  try {
    const { name, price } = req.body;
    
    const uploadedFile = req.files.image_url;

    const imageUrl = await productService.uploadFile(uploadedFile,uploadDir );

      productService.createProduct(name, price, imageUrl)
        .then(product => {
          res.json(successResponse('Product created successfully', { product }));
        })
        .catch(error => {
          console.error(error);
          res.status(500).json({ message: 'Product creation error' });
        });
   
  } catch (error) {
    console.error(error);
    res.status(500).json(errorResponse(error.message ));
  }
}

async function getAllProducts(req, res) {
  try {
    
    const products = await productService.getAllProducts();
    res.json(successResponse('Product fetched successfully', { products }));;
  } catch (error) {
    console.error(error);
    res.status(500).json(errorResponse(error.message ));
  }
}

async function getProductById(req, res) {
  try {
    const id = req.params.id;
    const product = await productService.getProductById(id);
    res.json(successResponse('Product fetched successfully', { product }));;
  } catch (error) {
    console.error(error);
    res.status(500).json(errorResponse(error.message ));
    // res.status(500).json({ message: error.message });
  }
}

async function updateProductById(req, res) {
  try {
    const id = req.params.id;
    const { name, price } = req.body;

    const productData = {
      id: id,
      name: name,
      price: price,
      imageUrl: ''
    };
    const uploadedFile = req.files.image_url;

    const oldProdcut  = await productService.getProductById(productData.id)
    if (oldProdcut == null)
    {
      throw new Error('Product not found to update ');
    }
     productData.imageUrl = oldProdcut.image_url;

    if (uploadedFile != null)
    {
      await productService.deleteFile('./public' + oldProdcut.image_url);
      productData.imageUrl = await productService.uploadFile(uploadedFile,uploadDir );
    }
  
    const products = await productService.updateProductById(productData); 
    res.json(successResponse('Product fetched successfully', { products }));;
  } catch (error) {
    console.error(error);
    res.status(500).json(errorResponse(error.message ));
  }
}

async function deleteProductById(req, res) {
  try {
    const id = req.params.id;
    const products = await productService.getProductById(id);
    await productService.deleteProductById(id);
    res.json(successResponse('Product Deleted successfully',null));;
  } catch (error) {
    console.error(error);
    res.status(500).json(errorResponse(error.message ));
  }
}
// Implement getProductById, updateProductById, and deleteProductById similarly

module.exports = {
  createProduct,
  getAllProducts,
  getProductById,
  updateProductById,
  deleteProductById,
  // Other controller functions
};