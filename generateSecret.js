const crypto = require('crypto');
const fs = require('fs');

// Generate random bytes for JWT secret
const jwtSecret = crypto.randomBytes(32).toString('hex');

// Write the secret to .env file
fs.writeFileSync('.env', `JWT_SECRET=${jwtSecret}\n`);
