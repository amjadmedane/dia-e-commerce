const express = require('express');
const fileUpload = require('express-fileupload');
const jwt = require('jsonwebtoken');
const User = require('./src/Models/user');
const productRoutes = require('./src/Routes/products');
const authRoutes = require('./src/Routes/auth');
require('dotenv').config();
const { verifyToken, requireAdmin } = require('./src/Middleware/middleware'); // Import middleware functions

const PORT = 8000;
const JWT_SECRET = process.env.JWT_SECRET;
var bodyParser = require('body-parser');

const app = express();

app.use(express.static('public'));
app.use(express.static('public/uploads/productImages'));

app.use(fileUpload());
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true })); 

var cors = require('cors')
app.use(cors())


app.use('/', authRoutes);

app.use('/products',verifyToken, productRoutes);

// Protected endpoint
app.get('/protected', verifyToken, (req, res) => {
  res.json({ message: 'This is a protected endpoint' });
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
