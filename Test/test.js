var assert = require('assert');
const {login} = require('../src/Controllers/userController');
describe('user', function () {
  describe('#login()', function () {
    it('login done', async function () {

        const req = { body: { username: "amjad", password: "1234567" } };
        const res = {
                    json: function(response) {
                        this.result = response;
                    },
                    status: function(statusCode) {
                        this.statusCode = statusCode;
                        return this;
                    }
                };
                await login(req, res);
      assert.equal(res.result.success, true);
    });
  });
});